Resumen:

		El proyecto consiste en la presentación de un programa capaz de descargar un archivo.pdb desde la base 
	de datos pdb.org, discriminar la información necesaria de la innecesaria para organizarla y graficarla de 
	manera más acotada.
	
Funciones:

	-Descarga de archivos PDB desde el servidor de rcsb.org.
	-Discriminación de la información en archivos más acotados.
	-Análisis de estos archivos.
	-Organización de la información de estos archivos.
	-Generación de los gráficos correspondientes a los ligandos de la proteína.

Instrucciones:

		Para iniciar el programa es necesario llamarlo desde terminal como "bash proyecto.sh parámetro", siendo "pará-
	metro" el código alfanumérico que se utiliza para identificar la proteína en la base de datos. Luego el programa 
	pide al usuario que ingrese el rango desde el centro de cada ligando en base cual el programa debe buscar los ele-
	mentos que rodean a los ligandos de la proteína.
