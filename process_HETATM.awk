BEGIN{
	
	#se declaran los valores iniciales de las variables que serán necesarias para
	#obtener los centros geométricos de los ligandos.
	
	#se le da a esta variable el valor del residuo de la primera linea (para entrar directamente al if).
    numero_de_residuo_anterior = $6
    
    #se declaran las variables que corresponden a las sumatorias de las coordenadas de los ligandos.
    ScoordenadaX=0
    ScoordenadaY=0
    ScoordenadaZ=0
    
    #se declara un contador para saber cuántos átomos componen a cada ligando.
    cantidad_de_atomos_por_ligando=0
    	
}
{

	if ( numero_de_residuo_anterior = $6 ){
		
		#se suman las coordenadas cada vez que el residuo anterior sea igual al actual.
	
		ScoordenadaX = ScoordenadaX + $7
		ScoordenadaY = ScoordenadaY + $8
		ScoordenadaZ = ScoordenadaZ + $9
		cantidad_de_atomos_por_ligando = cantidad_de_atomos_por_ligando + 1		
		
				
	else
		
		#una vez que se llega a un residuo nuevo las sumatorias se dividen por la cantidad de átomos que componen al ligando
		#para obtener el dato central de este.
		
		CentroX = (ScoordenadaX / cantidad_de_atomos_por_ligando)
		CentroY = (ScoordenadaY / cantidad_de_atomos_por_ligando)
		CentroZ = (ScoordenadaZ / cantidad_de_atomos_por_ligando)
		print CentroX, CentroY, CentroZ
	}
	
	#al final del ciclo se actualiza el residuo anterior.
	numero_de_residuo_anterior = $6
	
}

